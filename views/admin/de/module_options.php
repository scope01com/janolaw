<?php declare(strict_types=1);
// backward compability compatibility to 4.7.0
// include translations from new directory
require_once(\OxidEsales\Eshop\Core\Registry::getConfig()->getActiveView()->getViewConfig()->getModulePath('janolaw') . "application/views/admin/de/module_options.php");
?>